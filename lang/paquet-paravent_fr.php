<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'paravent_description' => 'Surcharge du plugin en travaux ouvrant le site aux auteur·es identifié·es',
	'paravent_nom' => 'Paravent',
	'paravent_slogan' => 'Un écran en cours de construction paramétrable',
);